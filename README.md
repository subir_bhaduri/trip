# Tactile refreshable image pad - TRIP

An electronically and pneumatically controlled wax tablet for tactile communication of images and texts to people with visual impairments. 